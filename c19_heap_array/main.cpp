#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    double *pArray{new double[13]{1, 3.1}};
    if (pArray != nullptr)
    {
        for (size_t i = 0; i < 13; i++)
        {
            cout << "index : " << i << " value : " << pArray[i] << endl;
        }
    }
    double arr[13]{123, 34, 2};
    cout << "arr sizeof : " << sizeof(arr) / sizeof(arr[0]) << " p Array sizeof : " << sizeof(pArray) / sizeof(pArray[0]) << endl;

    // c++
    cout << "arr size : " << size(arr) << endl;

    for (auto i : arr)
        cout << i << endl;
    delete[] pArray;
    pArray = nullptr;

    if (pArray != nullptr)
    {
        for (size_t i = 0; i < 13; i++)
        {
            cout << "index : " << i << " value : " << pArray[i] << endl;
        }
    }
    cout << "----- yz ------" << endl;
    return 0;
}
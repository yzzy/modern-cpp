#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    /*
    // const 与指针
    double n{1.4};
    const double *p{&n};
    double m{1.5};
    cout << "p address: " << p << endl;
    p = &m;

    cout << "p address: " << p << endl;
    */
    double n{1.4};
    double *const p{&n};
    double m{1.5};
    cout << "p address: " << p << endl;
    // p = &m;
    *p = m;
    cout << "p address: " << p << endl;
    cout << "p " << *p << endl;

    const double *const const_p{&n};
    // *const_p = m;
    // const_p = &m;
    double q{1.8};
    double &ref_n{n};
    ref_n = q;
    cout << n << ":" << &n << ":" << ref_n << endl;

    cout << "----- yz ------" << endl;
    return 0;
}
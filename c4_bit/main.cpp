#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    int a{5};
    int b{3};

    // and
    int aAndB{a & b};
    cout << "a & b = " << aAndB << endl;
    // or
    int aOrB{a | b};
    cout << "a | b = " << aOrB << endl;
    // xor
    int aXorB{a ^ b};
    cout << "a ^ b = " << aXorB << endl;
    // ~
    int x1{0};
    cout << (~x1) << endl;
    int x2{1};
    cout << (~x2) << endl;

    // 位移
    int left = {a << b}; // *8
    cout << "left " << left << endl;

    int right = {a >> b}; // /8
    cout << "right " << right << endl;
    cout << "----- yz ------" << endl;
}